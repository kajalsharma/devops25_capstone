**#Author - Ranjith & Kajal

#Updated Date - 20/09/2020

#Project Name - CapstoneProject

#Description - ShopIeasy is an E-Commerce Mobile Shopping Website developed Using Spring MVC and Selenium with WebDriver, TestNG Framework for Unit and Integration Testing. The site allows user to shops for Mobile online. User can navigate across various links on the page and browse the number of products for purchasing them. User can create his user profile and can login, perform actions and logout on the application. On having any doubts/enquiry on the product, user can also contact us by submitted his request online through our Contact Us Page.**


**#How to Use ?
#SetUp & Installation - To setup this project, You must have the following tools **
-

1.Jdk - set Env variable once after installation
2.Eclipse/Any IdE
3.Import this project from Eclipse - File Option
4.Once Imported, open the project and add required jar/Libraries mentioned in Pom.xml or run mvn clean package.
5.Make sure All error are gone after importing the project and adding neccessary jars and exe files.
6. Please add chrome, firefox,IE, Safari, Edge browser exe file in com.external folder.

**#Understanding the Project**

1. com.external package contains all exe browser drivers
2. com.pages - include page class for the project
3. com.testclass - include the test class for the Page
4. setupFiles - include the test setup files, all basic browser configurations
5. Configs - folder containing the property file to read common data from.
6. TestNG - jar added to the project
7. Referenced Libraries - all necessart jars are added here.
8. com.controller, com.dao,com.model,com.service - includes developement code for the Application using Spring MVC
9. target and test-output - are created when project is ran, these includes Project Reports - Unit, Integration Testing, Snapshots, logs and other details.
10. pom.xml - includes all the dependencies and plugins required for project.
11. testng.xml - execution file which includes the configuration of test classes, needed to execute as part of testing.


**#Running the Project.**

As the project is based on POM model, follow below steps to run it.
1. Make sure there are no errors, clean the project and refresh it.
    Run commands - mvn clean - cleans projects issues
                 - mvn install - install plugins and dependencies
                 - mvn test  - execute Unit and Integration test
    Once all errors and dependencies are resolved, run command 
                 - mvn clean package
2. Make sure to view the result in the Console of the Eclipse.
3. Open target folder to view surefire reports and logs
4. Open test-output to view testing results,logs and snapshots.

_Happy Development and Testing :)_****


